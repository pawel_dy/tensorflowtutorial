import numpy.random as rnd
import numpy as np
import matplotlib.pyplot as plt
import time





class MLP():
    warstwy = []
    siec=[]

    
    def MLP(self):
        print("utworzono perceptron: ",kom)

    def fit(self,x,y,dane,liczba_epok,warstwy):
        
        self.warstwy = warstwy
        self.utworz_siec()
        self.losuj_wagi()


        
    def predict(self):
        print("PREDYKCJA")
        
    def utworz_siec(self):
        for i in range(0,len(self.warstwy)):
            self.siec.append([])
        for i in range(0,len(self.warstwy)):
            for j in range(0,self.warstwy[i]):
                self.siec[i].append(Perceptron())
        print("Liczba neuronow w warstwach: ",self.warstwy)

    def feed_forward(self):
        print("Feed-forward")
    def back_propagation(self):
        print("Back-propagation")
    def losuj_wagi(self):
        print("losowanie wag")


class Perceptron():
    
    w_macierz = []
    error_list = []
    nr_perceptronu = 0
    
    def Perceptron(self):
        self.nr_perceptronu = self.nr_perceptronu + 1
        print('P: ',self.nr_perceptronu)
        
    def fit(self,dane,x,y,liczba_epok,wsp_ucz=0.1):
        print("Ropoczynam uczenie.")
        
        #dane = np.append(x,y.T,axis=1)
        
        self.error_list = []

        suma_errorow = 0


        liczba_cech = x.shape[1]

        liczba_probek = len(x)
        #w=rnd.random(liczba_cech+1)
        w = rnd.normal(size=liczba_cech+1)

        start = time.time()

        jedynki = np.ones((liczba_probek,1))


        self.w_macierz = np.ones((liczba_probek,liczba_cech+1))
        self.w_macierz[:,:] = w

        
        for i in range(0,liczba_epok):

            np.random.shuffle(dane)
            x = dane[:,0:liczba_cech]
            
            y = dane[:,-1]
            
            x2 = np.append(jedynki,x,axis=1)
            
            mnozenie = self.w_macierz*x2
            suma = mnozenie.sum(axis=1)
            
            
            wyjscia = f_akt(1,suma)

            error = wyjscia - y

            suma_errorow = sum(error)
            
            suma_x_er = error.dot(x2)

            self.w_macierz = self.w_macierz - wsp_ucz*suma_x_er*np.ones((liczba_probek,liczba_cech+1))
            
            sredni_error = (suma_errorow**2)/liczba_probek
            self.error_list.append(sredni_error)
        print("Koniec nauki")
        
    def predict(self,x):
        print("Rozpoczynam predykcje.")

        w = self.w_macierz[0,:]
        liczba_probek = len(x)
        liczba_cech = x.shape[1]
        self.w_macierz = np.ones((liczba_probek,liczba_cech+1))

        self.w_macierz[:,:] = w
        jedynki = np.ones((liczba_probek,1))
        
        x = np.append(jedynki,x,axis=1)
        mnozenie = self.w_macierz*x
        suma = mnozenie.sum(axis=1)
        wyjscia = f_akt(1,suma)
        return wyjscia
        


def normalizuj_dane(x,typ):
    #tutej jakis komentarz
    print("NORMALIZACJA")

    if typ == 'z-score':
        x = (x-np.mean(x))/np.std(x)
    elif typ == 'min-max':
        x = ( x-x.min(axis=0)) /(x.max(axis=0)-x.min(axis=0))
    return x


class Dataset():
    x = []
    y = []


def generuj_punkty(liczba_punktow,plot=False):
    mnoznik = 10
    x = mnoznik*(rnd.random(liczba_punktow)-0.5)
    x2 = np.array(x)*np.array(x)

    y = mnoznik*(rnd.random(liczba_punktow)-0.5)
    y2 = np.array(y)*np.array(y)


    a = 1.5
    b = 2
    a2 = 7
    b2 = -7
    
    label = np.zeros(liczba_punktow)

    #reguła przydzielania etykiet
    warunek1 = a*x*x*x+b2*np.sin(x)+b
    #warunek1 = a*x + b
    if plot == True:
        plt.plot(x[y<warunek1],y[y<warunek1],'*r')
        plt.plot(x[y>=warunek1],y[y>=warunek1],'*b')

    label[y<warunek1] = 1
    label[y>=warunek1] = 0

    dane = np.array([x,y,label])
    dane = dane.T
    
    #new_dataset = Dataset()
    #new_dataset.x = np.array([x,y])
    #new_dataset.y = np.array(label)
    
    return dane


def f_akt(typ,suma,alpha=0.1):
    
    if typ == 0:
        if suma>0:
            wyjscie = 1
        else:
            wyjscie = -1
    elif typ == 1:
        
        wyrazenie = -suma*alpha
        
        wyjscie = 1.0/(1+np.exp(wyrazenie))
    return wyjscie
